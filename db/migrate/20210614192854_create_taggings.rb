class CreateTaggings < ActiveRecord::Migration[6.1]
  def change
    create_table :taggings do |t|
      t.references :tag, null: true , foreign_key: true
      t.references :article, {null: true , foreign_key: true }

      t.timestamps
    end
  end
end
