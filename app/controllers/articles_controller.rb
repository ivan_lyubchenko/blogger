class ArticlesController < ApplicationController
  include ArticlesHelper
  before_action :require_login, except:  [:index, :show]
  def index
    @articles = Article.all
  end

  def show
    @article = Article.find(params[:id])

    @comment = Comment.new
    @comment.article_id = @article.id

  end

  def new
    @article = Article.new
  end

  def create
    article = Article.create(article_params)
    flash.notice = "Article has successfully created"
    redirect_to article
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.update(article_params)
    flash.notice = "Article has successfully updated"
    redirect_to article_path(@article)
  end

  def destroy
    Article.destroy(params[:id])

    flash.notice = "Article has successfully destroyed"
    redirect_to articles_path
  end
end
